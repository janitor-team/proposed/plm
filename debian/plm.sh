#!/bin/sh
#
#

set -e

. /usr/lib/java-wrappers/java-wrappers.sh

# Jython compiles java bytecode into python bytecode in advance; it needs a user-writable directory to cache it
export JYTHON_OPTS="${JYTHON_OPTS} -Dpython.cachedir=${HOME}/.jython_cachedir"

# Detect and warn about a possible misconfiguration
if [ -n "$JAVA_HOME" ] && [ "x$JAVA_HOME" != x. ] ; then
	echo "Your JAVA_HOME is set to $JAVA_HOME. Please make sure that this points to a Java7 runtime"
fi	
if [ -n "$JAVA_CMD" ] && [ "x$JAVA_CMD" != x. ] ; then
	echo "Your JAVA_CMD is set to $JAVA_CMD. Please make sure that this points to a Java7 runtime"
fi	

# We depend on the openjdk runtime 
find_java_runtime

# Request jython and friends.
# (I'd like to have it used only if found, but this does not work yet: as we build-dep on jython, jh_build adds it to the depends)
find_jars jython jruby antlr3-runtime-3.2
# jython fails with antlr3-runtime-3.5 (see #796933)

# For some reason, some people need these jar to be manually loaded. That's annoying, they should be picked automatically instead.
find_jars commons-logging guava

# http stuff
find_jars httpmime httpcore httpclient

# scala stuff
# find_jars scala-compiler (not in the right directory)
JAVA_CLASSPATH=$JAVA_CLASSPATH:/usr/share/scala/lib/scala-compiler.jar
find_jars scala-library scala-reflect

# We obviously need our own library
find_jars plm

run_java plm.core.ui.ProgrammersLearningMachine "$@"
